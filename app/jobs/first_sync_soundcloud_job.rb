class FirstSyncSoundcloudJob < ActiveJob::Base
  queue_as :default

  def perform(user)
    @user = user
    @client = Soundcloud.new(client_id: 'bb2c55dd7bc8bc59ad98863c0c39bd8c',
                             client_secret: 'c65cf3f8b3f5e7814573f0d42d35e06d')

    Track.soundcloud_grub_tracks @user, @client

  end
end
