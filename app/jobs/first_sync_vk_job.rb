class FirstSyncVkJob < ActiveJob::Base
  queue_as :default

  def perform(user)

    @user = user

    Track.vk_grub_tracks @user

  end
end
