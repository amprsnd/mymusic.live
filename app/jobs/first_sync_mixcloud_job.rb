class FirstSyncMixcloudJob < ActiveJob::Base
  queue_as :default

  def perform(user)

    @user = user

    Track.mixcloud_grub_tracks @user

  end
end
