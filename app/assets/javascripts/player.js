var callbackFunc;

window._urq = window._urq || [];
_urq.push(['initSite', '6da38b6e-a599-4f75-985c-3dd0ff1fed1d']);
(function() {
    var ur = document.createElement('script'); ur.type = 'text/javascript'; ur.async = true;
    ur.src = ('https:' == document.location.protocol ? 'https://cdn.userreport.com/userreport.js' : 'http://cdn.userreport.com/userreport.js');
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ur, s);
})();

webix.ready(function () {

    // Folders
    var folders = {
        id: "folders",
        view:"tree",
        select: true,
        width: 170,
        maxWidth: 450,
        minWidth: 150,
        url: "player/folders.json",
        datatype: "json"
    };

    // Tracks
    var tracks = {
        id: "tracks",
        view:"dataview",
        template: "<img src='#artwork#' alt='' class='artwork' />" +
        "<i class='fa fa-#source#'></i>" +
        "<div class='artist'>#artist#:</div>" +
        "<div class='title'>#title#</div>" +
        "<div class='duration'>#duration#</div>",

        drag:true,
        select:"multiselect", navigation:true,
        url: "player/tracks.json",
        datatype: "json",
        sort: {
            by: 'order',
            dir: 'asc'
        },
        type: {
            css: 'one_track',
            width: 150,
            height: 200
        }
    };

    // Queue
    var queue = {
        id: "queue",
        view:"datatable",
        columns:[
            { id:"index", header:"", sort:"int", width: 40},
            { id:"source", header:"", width: 30, template: "<i class='fa fa-#source#'></i>", css: "source"},
            { id:"track",
                header:"Track",
                width:200,
                fillspace:true,
                template: "<span class='artist'>#artist#</span> - #title#",
                css: "track"
            },
            { id:"duration", header:"", width: 70}
        ],
        select:"multiselect",
        drag: true,
        scrollX: false,
        url: "player/queue.json",
        //save: "player/queue",
        datatype: "json",
        width: 400, maxWidth: 600, minWidth: 200,
        scheme:{
            $init:function(obj){ obj.index = this.count(); }
        },
        on:{
            onBeforeLoad:function(){
                this.showOverlay("Loading...");
            },
            onAfterLoad:function(){
                this.hideOverlay();
            }
        }
    };

    var mainPlayer = {view:"toolbar", paddingX: 10, paddingY:2, css: "mainPlayer",
        cols:[
            {width: 50, view: "icon", icon: "play",         id: "playButton"},
            {width: 50, view: "icon", icon: "pause",        id: "pauseButton", hidden: true},
            {width: 50, view: "icon", icon: "stop",         id: "stopButton"},
            {width: 50, view: "icon", icon: "backward",     id: "prevButton"},
            {width: 50, view: "icon", icon: "forward",      id: "nextButton"},
            {view: "slider", value: "0", min: 0, max: 100,  id: "trackProgress"},
            {width: 150, view: "slider", value: "50", min: 0, max: 100, id: "trackVolume", minHeight: 50},
            {width: 50, view: "icon", icon: "info-circle",  id: "infoButton"}
        ],
        height: 50
    };

    var header = {view:"toolbar", paddingX: 10, paddingY:2, css: "playerHeader",
        cols:[
            {template:"<img src='/images/logo.png' alt=''>", width: 50, css: 'player_logo' },
            {template:"<span>v0.1</span>", height: 50, css: 'player_version' },
            {width: 50, view: "icon", icon: "info",         id: "projectInfo", click: "window.open('/about', '_blank')"},
            {width: 50, view: "icon", icon: "comments-o ",  id: "feedback", click: "_urq.push(['Feedback_Open'])"},
            {width: 50, view: "icon", icon: "sign-out",     id: "signOut", click: "window.open('/auth/logout', '_blank')"}

        ],
        height: 50
    };


    // Layout
    webix.ui({
        type:"space",
        padding: 0,
        id:"layout",
        responsive:"layout",
        rows:[
            header,
            {cols:[
                folders,
                {view:"resizer", id:"resizer1"},
                tracks,
                {view:"resizer", id:"resizer2"},
                queue
            ]
            },
            mainPlayer
        ]
    }).show();
    $$('tracks').sort('sort', 'asc');

    // Folders
    $$('folders').attachEvent('onAfterSelect', function (id) {

        $$('tracks').clearAll();
        $$('tracks').load('player/tracks.json?folder=' + id, 'json', function(){
            //console.log('click! ' + id);
        });


    });

    // Tracks
    $$('tracks').attachEvent("onBeforeAdd", function(){
        return false;
    });

    $$('tracks').attachEvent("onBeforeDelete", function(){
        return false;
    });


    // Queue
    $$('queue').attachEvent('onAfterDelete', function (id) {

        var selectedItems = $$('queue').getSelectedId(true).length;
        if (selectedItems <= 0) {
            //console.log("delete");
            syncQueue();
        }

    });

    $$('queue').attachEvent('onAfterDrop', function () {
        //console.log("sort");
        syncQueue();
    });


    // Main Music Player ==================================
    var trackStatus = ['stopped'];
    // statuses:
    //    stopped
    //    played
    //    paused
    var trackTime = [],
        trackProgress = 0;

    // Play
    var trackWidget;

    $$('queue').attachEvent('onItemDblClick', function (id, e, node) {

        var tracks = $$('queue').getSelectedItem(true);
        trackStatus[2] = tracks[0];
        startTrack(trackStatus[2]);

    });

    $$('playButton').attachEvent('onItemClick', function () {

        if (trackStatus[0] == 'stopped') {

            var tracks = $$('queue').getSelectedItem(true);
            trackStatus[2] = tracks[0];
            startTrack(trackStatus[2]);

        } else {
            playTrack();
        }

    });

    $$('pauseButton').attachEvent('onItemClick', function () {
        pauseTrack();
    });

    $$('stopButton').attachEvent('onItemClick', function () {
        stopTrack();
    });

    $$('prevButton').attachEvent('onItemClick', function () {
        if (trackStatus[0] != 'stopped') {
            previousTrack();
        }
    });

    $$('nextButton').attachEvent('onItemClick', function () {
        if (trackStatus[0] != 'stopped') {
            nextTrack();
        }
    });

    $$('infoButton').attachEvent('onItemClick', function () {
        infoTrack();
    });

    $$("trackProgress").attachEvent("onChange", function(value){
        //setProgress(value);
    });

    $$("trackProgress").attachEvent("onSliderDrag", function(){
        setProgress();
    });

    $$("trackVolume").attachEvent("onSliderDrag", function(){
        var value = this.getValue();
        setVolume(value);
    });

    function startTrack(track) {

        var currentTrack;

        //console.log('PLAY! ' + track.track_id + ' / ' + track.track_url + ' / ' + track.source);

        switch (track.source) {
            case 'soundcloud':

                $('#musicWidget').html('<iframe id="trackWidget" width="100%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url='+ track.track_url +'&amp;color=0066cc&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>');

                currentTrack = SC.Widget($('#trackWidget')[0]);

                trackStatus[1] = 'soundcloud';

                currentTrack.bind(SC.Widget.Events.READY, function() {

                    //currentTrack.stop();
                    currentTrack.play();

                    currentTrack.bind(SC.Widget.Events.PLAY, function () {

                        //volume
                        var volume = $$("trackVolume").getValue() / 100;
                        currentTrack.setVolume(volume);

                        currentTrack.getDuration(function (d) {
                            trackTime[1] = d;
                            trackTime[2] = trackTime[1] / 100;
                        });

                    });

                    currentTrack.bind(SC.Widget.Events.PLAY_PROGRESS, function () {

                        currentTrack.getPosition(function (p) {
                            trackTime[0] = p;
                            trackProgress = Math.round(trackTime[0] / trackTime[2]);
                            $$('trackProgress').setValue(trackProgress);
                        });

                    });

                    currentTrack.bind(SC.Widget.Events.FINISH, function () {
                        nextTrack();
                    });

                });


                break;
            case 'mixcloud':

                $('#musicWidget').html('<iframe id="trackWidget" width="100%" height="150" src="https://www.mixcloud.com/widget/iframe/?feed='+ track.track_url +'&hide_cover=1&autoplay=1" frameborder="0"></iframe>');

                currentTrack = Mixcloud.PlayerWidget($('#trackWidget')[0]);

                trackStatus[1] = 'mixcloud';

                currentTrack.ready.then(function() {

                    //volume
                    var volume = $$("trackVolume").getValue() / 100;
                    currentTrack.setVolume(volume);

                    currentTrack.events.ended.on(function () {
                        nextTrack();
                    });

                    currentTrack.events.progress.on(function () {

                        currentTrack.getDuration().then(function (d) {
                            trackTime[1] = d;
                            trackTime[2] = trackTime[1] / 100;
                        });

                        currentTrack.getPosition().then(function(p) {
                            trackTime[0] = p;
                            trackProgress = Math.round(trackTime[0] / trackTime[2]);
                            $$('trackProgress').setValue(trackProgress);
                        });

                    });

                });

                break;
            case 'vk':

                var trackUrl = 'https://api.vk.com/method/audio.get?audio_ids=' + track.outer_id + '&access_token=' + vkUserToken;
                var script = document.createElement( 'script' );
                script.type = 'text/javascript';
                script.src = trackUrl + '&callback=callbackFunc';

                $('#musicWidget').html(script);
                callbackFunc = function (result) {
                    $('#musicWidget').append('<audio id="trackWidget" autoplay="autoplay" src="' + result.response[0].url + '"><source src="' + result.response[0].url + '"></audio>');
                    $('#musicWidget').append('<p style="text-align: center; padding: 20px 0">No additional info for this track</p>');
                    currentTrack = $('#trackWidget')[0];
                    trackWidget = $('#trackWidget')[0];

                    //volume
                    currentTrack.volume = $$("trackVolume").getValue() / 100;

                    currentTrack.ontimeupdate = function () {
                        trackTime[0] = currentTrack.currentTime;
                        trackTime[1] = currentTrack.duration;
                        trackTime[2] = trackTime[1] / 100;
                        trackProgress = Math.round(trackTime[0] / trackTime[2]);
                        $$('trackProgress').setValue(trackProgress);

                    };

                    currentTrack.onended = function () {
                        //console.log('Ended!');
                        nextTrack();
                    }

                };





                trackStatus[1] = 'vk';

                break;
            default:
        }

        $$('playButton').hide();
        $$('pauseButton').show();

        $$('queue').clearCss("current_play");
        $$('queue').addRowCss(track.id, "current_play");

        trackWidget = currentTrack;
        trackStatus[0] = 'played';

    }

    function playTrack() {

        trackWidget.play();
        //console.log('RESUME.');

        $$('playButton').hide();
        $$('pauseButton').show();

        trackStatus[0] = 'played';

    }

    // Pause
    function pauseTrack() {

        trackWidget.pause();
        //console.log('PAUSE.');

        $$('pauseButton').hide();
        $$('playButton').show();

        trackStatus[0] = 'paused';

    }

    // Stop
    function stopTrack() {

        $('#trackWidget').remove();
        //console.log('STOP!');

        trackStatus[0] = 'stopped';

        $$('queue').clearCss("current_play");

        $$('pauseButton').hide();
        $$('playButton').show();

        $$('trackModal').setPosition(0,-360);
        $$('trackProgress').setValue(0);

    }

    // Previous
    function previousTrack() {

        var currentTrack = trackStatus[2],
            prevTrackIndex = (currentTrack.index - 2),
            prevTrack;

        if (prevTrackIndex < 0) {
            prevTrack = $$('queue').getItem($$('queue').getLastId());
        } else {
            prevTrack = $$('queue').getItem($$('queue').getIdByIndex(prevTrackIndex));
        }

        trackStatus[2] = prevTrack;
        startTrack(trackStatus[2]);
        trackStatus[0] = 'played';

    }

    // Next
    function nextTrack() {

        var currentTrack = trackStatus[2],
            nextTrackIndex = (currentTrack.index),
            lastItem = $$('queue').getItem($$('queue').getLastId()),
            nextTrack;

        if ((nextTrackIndex + 1) > lastItem.index) {
            nextTrack = $$('queue').getItem($$('queue').getFirstId());
        } else {
            nextTrack = $$('queue').getItem($$('queue').getIdByIndex(nextTrackIndex));
        }



        trackStatus[2] = nextTrack;
        startTrack(trackStatus[2]);
        trackStatus[0] = 'played';

    }

    // Info
    function infoTrack() {

        $$('trackModal').setPosition(0,0);

    }

    // Set progress
    function setProgress() {

        var seekTo;

        if (trackStatus[0] == 'played') {

            if (trackStatus[1] == 'soundcloud') {
                seekTo = Math.round(trackTime[2] * $$('trackProgress').getValue());
                trackWidget.seekTo(seekTo);
                //console.log('SET PROGRESS TO: ' + seekTo);
            }

            if (trackStatus[1] == 'mixcloud') {
                seekTo = Math.round(trackTime[2] * $$('trackProgress').getValue());
                trackWidget.seek(seekTo);
                //console.log('SET PROGRESS TO: ' + seekTo);
            }

            if (trackStatus[1] == 'vk') {
                seekTo = Math.round(trackTime[2] * $$('trackProgress').getValue());
                trackWidget.currentTime = seekTo;
                //console.log('SET PROGRESS TO: ' + seekTo);
            }


        }


    }

    function setVolume(value) {

        if (trackStatus[0] == 'played') {
            if (trackStatus[1] == 'vk') {
                trackWidget.volume = (value / 100);
            } else {
                trackWidget.setVolume(value / 100);
            }
        }

        //console.log('SET VOLUME TO: ' + value / 100);
    }

    function syncQueue() {

        var q = $$('queue');
        var order = 1;
        q.data.each(
            function(obj){
                obj.index = order;
                order++
            }
        );

        var range = q.data.pull;
        //console.log(range);

        q.refresh();

        webix.ajax().post("/player/queue", {queue: JSON.stringify(range)}, function(text, xml, xhr){
            //response
            //console.log(text);
        });


    }

    var windowTemplate =  "<div id='musicWidget'></div>" +
        "<div id='trackArtist'></div>" +
        "<div id='trackTitle'></div>" +
        "<div id='trackduration'></div>";

    webix.ui({
        view:"window",
        id:"trackModal",
        position: function(state){
            state.left = 0;
            state.top = -210;
            state.width = state.maxWidth ;
            state.height = 200;
        },
        head:{
            view:"toolbar", margin: -4, cols:[
                {view:"label", label: "Track info" },
                { view:"icon", icon:"angle-up",
                    click:"$$('trackModal').setPosition(0, -360);"}
            ]
        },
        body: windowTemplate
    }).show();



});
