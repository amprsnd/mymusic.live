$(function () {

    // User report modal
    $(document).on('click', '.ur', function (e) {
        $('#ur-container').append('<iframe src="https://feedback.userreport.com/6da38b6e-a599-4f75-985c-3dd0ff1fed1d/"></iframe>');
        $('#ur').openModal();
        e.preventDefault();
    });

    $(document).on('click', '.ur-close', function (e) {
        $('#ur').closeModal();
        $('#ur-container iframe').remove();
        e.preventDefault();
    });


    //notices
    var Message = $('.system-message');

    //Show notice message
    Message.css({
        opacity : 1,
        top : 0
    });

    //autohide
    //TODO: if error - disable autohide
    if (Message.length != 0) {
        setTimeout(function(){
            Message.css('opacity', '0');
            setTimeout(function(){
                Message.remove();
            }, 501);
        }, 3500);
    }

    //close notice
    $('.close-this').on('click', function() {
        var closeButton = $(this);
        closeButton.parent().css('opacity', '0');
        setTimeout(function(){
            closeButton.parent().remove();
        }, 501);
    });

    $('.materialboxed').materialbox();

});


var ready;
ready = function() {

};

$(document).ready(ready);
$(document).on('turbolinks:load', ready);

