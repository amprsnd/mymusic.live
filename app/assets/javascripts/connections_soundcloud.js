$(function () {

    if (window.location.hash && sc_connect_page) {

        var params = window.location.hash.split(/[=&]/);
        if (params[0] == '#access_token') {
            var access_token = params[1];
            $.post( "/connections/soundcloud_auth", { access_token: access_token } );
            console.log('send Access Token');
        }

    }

});
