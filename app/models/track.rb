class Track < ActiveRecord::Base

  # validations

  # relations
  has_and_belongs_to_many :folders, :uniq => true

  has_many :user_tracks
  has_many :users, through: :user_tracks

  # paperclip
  has_attached_file :artwork,
                    styles: {
                      medium: '320x320>',
                      small: '100x100>'
                    },
                    default_url: '/images/cover.png',
                    path: ':rails_root/public/covers/:style_:filename'
  validates_attachment_content_type :artwork, content_type: /\Aimage\/.*\Z/

  # Methods
  # SoundCloud =====================================================

  def self.soundcloud_grub_tracks(user, client)

    @user = user
    @client = client

    # folders manipulations
    root = Folder.where(title: 'SoundCloud', user_id: @user.id).first_or_create do |folder|
      folder.title = 'SoundCloud'
      folder.user_id = @user.id
    end
    #root.present? ? '' : SoundcloudTrack.gen_new_folders %w(MyLikes MyReposts Groups), @user, root


    # liked tracks
    my_likes = Folder.generate_subfolder('MyLikes', @user, root)

    @a = @client.get("/users/#{@user.soundcloud_id}/favorites", linked_partitioning: 1, limit: 100)
    Track.get_likes @a, my_likes

    # liked playlists
    @b = @client.get("/e1/users/#{@user.soundcloud_id}/playlist_likes", linked_partitioning: 1, limit: 100)
    Track.soundcloud_likes_from_playlists @b, my_likes

    # groups tracks
    groups_folder = Folder.generate_subfolder('MyGroups', @user, root)

    @c = @client.get("/users/#{@user.soundcloud_id}/groups", limit: 10)
    Track.groups_tracks @c, groups_folder


  end

  # SoundCloud Tracks from likes
  def self.get_likes(collection, folder)

    collection['collection'].each do |track|
      Track.soundcloud_tracks track, folder
    end

    if collection['next_href'].present?

      collection = @client.get(collection['next_href'], linked_partitioning: 1)

      Track.get_likes collection, folder

    end

  end

  # SoundCloud Tracks from liked playlists TODO: DO REFACTOR!!!
  def self.soundcloud_likes_from_playlists(collection, folder)

    collection['collection'].each do |list|
      list['playlist']['tracks'].each do |track|

        Track.soundcloud_tracks track, folder

      end
    end

    if collection['next_href'].present?

      collection = @client.get(collection['next_href'], linked_partitioning: 1)

      Track.soundcloud_likes_from_playlists collection, folder

    end

  end

  # Soundcloud Groups tracks
  def self.groups_tracks(groups, folder)
    groups.each do |group|

      group_folder = Folder.generate_subfolder(group['name'], @user, folder)
      group_tracks = @client.get("/groups/#{group['id']}/tracks", limit: 10)

      group_tracks.each do |track|
        Track.soundcloud_tracks track, group_folder
      end

    end
  end

  # SoundCloud Tracks
  def self.soundcloud_tracks(current_track, current_folder)
    @track = Track.where('track_id = ? AND track_source = ?', current_track['id'], 'soundCloud').first_or_create do |sctrack|
      sctrack.track_id = current_track['id']
      sctrack.track_created_at = current_track['created_at']
      sctrack.track_duration = current_track['duration']
      sctrack.track_commentable = current_track['commentable']
      sctrack.track_state = current_track['state']
      sctrack.track_original_content_size = current_track['original_content_size']
      sctrack.track_updated_at = current_track['last_modified']
      sctrack.track_sharing = current_track['sharing']
      if current_track['tag_list'].present?
        sctrack.track_tag_list = current_track['tag_list'].truncate(512, omission: '...')
      end
      sctrack.track_permalink = current_track['permalink']
      sctrack.track_streamable = current_track['streamable']
      sctrack.track_embeddable_by = current_track['embeddable_by']
      sctrack.track_downloadable = current_track['downloadable']
      sctrack.track_purchase_url = current_track['purchase_url']
      sctrack.track_label_id = current_track['label_id']
      sctrack.track_purchase_title = current_track['purchase_title']
      sctrack.track_genre = current_track['genre']
      sctrack.track_title = current_track['title']
      if current_track['description'].present?
        sctrack.track_description = current_track['description'].truncate(512, omission: '...')
      end
      sctrack.track_label_name = current_track['label_name']
      sctrack.track_release = current_track['release']
      sctrack.track_track_type = current_track['track_type']
      sctrack.track_key_signature = current_track['key_signature']
      sctrack.track_isrc = current_track['isrc']
      sctrack.track_video_url = current_track['video_url']
      sctrack.track_bpm = current_track['bpm']
      sctrack.track_release_year = current_track['release_year']
      sctrack.track_release_month = current_track['release_month']
      sctrack.track_release_day = current_track['release_day']
      sctrack.track_original_format = current_track['original_format']
      sctrack.track_license = current_track['license']
      sctrack.track_uri = current_track['uri']
      sctrack.track_permalink_url = current_track['permalink_url']
      sctrack.track_artwork_url = current_track['artwork_url']
      #if current_track.artwork_url.present?
      #  sctrack.artwork = URI.parse current_track.artwork_url
      #end
      sctrack.track_waveform_url = current_track['waveform_url']
      sctrack.track_stream_url = current_track['stream_url']
      sctrack.track_attachments_uri = current_track['attachments_uri']
      sctrack.track_user_id = current_track['user_id']
      sctrack.track_artist = current_track['user']['username']
      sctrack.track_artist_url = current_track['user']['permalink_url']
      sctrack.track_artist_userpic = current_track['user']['avatar_url']
      sctrack.track_source = 'soundCloud'
    end
    # add track to folder
    current_folder.tracks << @track unless current_folder.tracks.include?(@track)

    # add track to user
    UserTrack.where(user_id: @user.id, track_id: @track.id).first_or_create do |association|
      association.user_id = @user.id
      association.track_id = @track.id
    end

  end


  #folders for track
  def self.gen_new_folders(folders, user, root)
    folders.each do |folder|
      Folder.generate_subfolder(folder, user, root)
    end
  end

  # SoundCloud end =================================================

  # Mixcloud =======================================================

  def self.mixcloud_grub_tracks(user)

    @user = user
    @client = user.mixcloud_id
    @api_uri = 'https://api.mixcloud.com'

    # folders manipulations
    root = Folder.where(title: 'MixCloud', user_id: @user.id).first_or_create do |folder|
      folder.title = 'MixCloud'
      folder.user_id = @user.id
    end

    # liked tracks


    @a = JSON.parse HTTParty.get("#{@api_uri}#{@client}favorites/")
    if @a['data'].present?
      my_likes = Folder.generate_subfolder('Favorites', @user, root)
      Track.get_mixcloud_likes @a, my_likes
    end


    # followings
    @b = JSON.parse HTTParty.get("#{@api_uri}#{@client}following/")
    @followings = []
    if @b['data'].present?
      my_followings = Folder.generate_subfolder('Followings', @user, root)

      collect_followings_profiles @b
      Track.get_followings @followings, my_followings
    end

    # cloudcasts
    #'https://api.mixcloud.com/ontariostreet/cloudcasts/'
    #'https://api.mixcloud.com/ontariostreet/favorites/'
    #"#{api_uri}#{@client}cloudcasts/"

  end

  # MixCloud Tracks from favorites
  def self.get_mixcloud_likes(collection, folder)

    if collection['data'].present?

      collection['data'].each do |track|
        Track.mixcloud_tracks track, folder
      end

      if collection['paging']['next'].present?
        collection = JSON.parse HTTParty.get(collection['paging']['next'])
        Track.get_mixcloud_likes collection, folder
      end

    end

  end

  def self.collect_followings_profiles(followings)
    followings['data'].each do |following|
      @followings << following['key']
    end

    if followings['paging']['next'].present?
      followings = JSON.parse HTTParty.get(collection['paging']['next'])
      collect_followings_profiles followings
    end

  end

  # MixCloud Tracks from followings
  def self.get_followings(collection, folder)

    collection.each do |follower|

      @f = JSON.parse HTTParty.get("#{@api_uri}#{follower}/")
      following_name = @f['name']

      # uploads
      @fa = JSON.parse HTTParty.get("#{@api_uri}#{follower}cloudcasts/")

      # favorites
      @fb = JSON.parse HTTParty.get("#{@api_uri}#{follower}favorites/?limit=10")

      # create folder
      if @fa['data'].present? || @fb['data'].present?
        @following_folder = Folder.generate_subfolder(following_name, @user, folder)
      end

      if @fa['data'].present?

        follower_cloudcasts = Folder.generate_subfolder('Cloudcasts', @user, @following_folder)
        Track.following_cloudcasts @fa, follower_cloudcasts

      end

      if @fb['data'].present?

        follower_favorites = Folder.generate_subfolder('Favorites', @user, @following_folder)
        @fb['data'].each do |track|
          Track.mixcloud_tracks track, follower_favorites
        end

      end

    end

  end

  # Each following user cloudcasts
  def self.following_cloudcasts(data, folder)
    data['data'].each do |track|
      Track.mixcloud_tracks track, folder
    end

    if data['paging']['next'].present?
      data = JSON.parse HTTParty.get(data['paging']['next'])
      Track.following_cloudcasts data, folder
    end

  end

  # MixCloud Tracks
  def self.mixcloud_tracks(current_track, current_folder)
    @track = Track.where('track_id = ? AND track_source = ?', current_track['key'], 'mixCloud').first_or_create do |mctrack|

      #mctrack.track_id
      mctrack.track_uri = current_track['key']
      mctrack.track_permalink = current_track['slug']
      mctrack.track_permalink_url = current_track['url']
      mctrack.track_title = current_track['name']
      mctrack.track_duration = current_track['audio_length']
      mctrack.track_artwork_url = current_track['pictures']['320wx320h']
      #if current_track['pictures']['320wx320h'].present?
      #  mctrack.artwork = URI.parse current_track['pictures']['320wx320h']
      #end
      #mctrack.mc_picture_primary_color = current_track['picture_primary_color']
      if current_track['description'].present?
        mctrack.track_description = current_track['description'].truncate(512, omission: '...')
      end
      mctrack.track_count_play = current_track['play_count']
      mctrack.track_count_favorite = current_track['favorite_count']
      mctrack.track_count_listener = current_track['listener_count']
      mctrack.track_count_repost = current_track['repost_count']
      mctrack.track_count_comment = current_track['comment_count']
      mctrack.track_created_at = current_track['created_time']
      mctrack.track_updated_at = current_track['updated_time']
      mctrack.track_artist_url = current_track['user']['url']
      mctrack.track_artist = current_track['user']['name']
      mctrack.track_artist_userpic = current_track['user']['pictures']['320wx320h']
      mctrack.track_source = 'mixCloud'
    end
    # add track to folder
    current_folder.tracks << @track unless current_folder.tracks.include?(@track)

    # add track to user
    UserTrack.where(user_id: @user.id, track_id: @track.id).first_or_create do |association|
      association.user_id = @user.id
      association.track_id = @track.id
    end

  end

  # Mixcloud end ====================================================

  # Vk ==============================================================

  def self.vk_grub_tracks(user)

    @user = user
    @client = VkontakteApi::Client.new(user.vk_token)

    # folders manipulations
    root = Folder.where(title: 'Vk', user_id: @user.id).first_or_create do |folder|
      folder.title = 'Vk'
      folder.user_id = @user.id
    end

    my_tracks_folder = Folder.generate_subfolder('MyTracks', @user, root)

    # Get user tracks
    @get_audio = HTTParty.get("https://api.vk.com/method/audio.get?owner_id=#{user.vk_id}&access_token=#{user.vk_token}")
    @audio = @get_audio['response'].drop(1)
    Track.vk_my_tracks @audio, my_tracks_folder

    # TODO: fix fuckin groups. ...if needed.
    # get list user groups
    #@groups = HTTParty.get("https://api.vk.com/method/groups.get?user_id=#{user.vk_id}&access_token=#{user.vk_token}")
    #Track.vk_groups @groups, @user, root

    # TODO: friends music

    #@vk = VkontakteApi::Client.new(user.vk_token)
    #@audio = @vk.audio.get
    #group = @vk.groups.get
    #@group_music = @vk.audio.get(owner_id: "-#{group}")

  end

  # Vk Tracks
  def self.vk_my_tracks(audio, folder)
    audio.each do |my_audio|
      Track.vk_tracks my_audio, folder
    end

  end

  def self.vk_groups(groups_list, user, root_folder)
    groups_list['response'].each do |group|

      Track.vk_one_group group, user, root_folder

    end
  end

  def self.vk_one_group(group_id, user, root_folder)
    # Group music
    get_group_music = HTTParty.get("https://api.vk.com/method/audio.get?owner_id=-#{group_id}&access_token=#{user.vk_token}")
    group_music = get_group_music['response']

    if group_music.present? && group_music[0] != 0

      # Group info
      get_group = HTTParty.get("https://api.vk.com/method/groups.getById?group_id=#{group_id}&access_token=#{user.vk_token}")
      group = get_group['response'][0]

      # Group folder
      group_folder = Folder.generate_subfolder(group['name'], user, root_folder)

      # Group music to database
      group_music.drop(1).each do |group_audio|
        Track.vk_tracks group_audio, group_folder
      end

    end
  end

  def self.vk_tracks(current_track, current_folder)
    @track = Track.where('track_id = ? AND track_source = ?', current_track['aid'], 'vk').first_or_create do |vktrack|

      vktrack.track_id = current_track['aid']
      vktrack.track_artist_url = current_track['owner_id']
      vktrack.track_artist = current_track['artist']
      vktrack.track_title = current_track['title']
      vktrack.track_duration = current_track['duration']
      vktrack.track_permalink_url = current_track['url']
      vktrack.track_uri = current_track['url']
      vktrack.track_genre = current_track['genre']
      vktrack.track_source = 'vk'

    end
    # add track to folder
    current_folder.tracks << @track unless current_folder.tracks.include?(@track)

    # add track to user
    UserTrack.where(user_id: @user.id, track_id: @track.id).first_or_create do |association|
      association.user_id = @user.id
      association.track_id = @track.id
    end

  end

  # Vk end ==========================================================

end
