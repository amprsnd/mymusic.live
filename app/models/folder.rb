class Folder < ActiveRecord::Base

  # validations

  # relations
  belongs_to :user

  # Old
  #has_and_belongs_to_many :soundcloud_tracks, :uniq => true
  #has_and_belongs_to_many :mixcloud_tracks, :uniq => true
  #has_and_belongs_to_many :vk_tracks, :uniq => true

  has_and_belongs_to_many :tracks, :uniq => true

  has_many :subfolder, class_name: 'Folder',
           foreign_key: 'supfolder_id'

  belongs_to :supfolder, class_name: 'Folder'

  # other


  # methods

  def self.generate_subfolder(title, user, root)
    self.where(title: title, user_id: user.id, supfolder_id: root).first_or_create do |folder|
      folder.title = title
      folder.user_id = user.id
      folder.supfolder_id = root.id
    end
  end

end
