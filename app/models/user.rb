class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #relations
  has_many :folders

  has_many :playlists

  has_many :user_tracks
  has_many :tracks, through: :user_tracks

  has_many :play_queue

  # Old
  has_many :user_soundcloud_tracks
  has_many :soundcloud_tracks, through: :user_soundcloud_tracks

  has_many :user_mixcloud_tracks
  has_many :mixcloud_tracks, through: :user_mixcloud_tracks

  has_many :user_vk_tracks
  has_many :vk_tracks, through: :user_vk_tracks

end
