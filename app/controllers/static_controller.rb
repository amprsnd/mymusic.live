class StaticController < ApplicationController
  def index

    @user = current_user
    if @user

      if @user.tracks.present?
        redirect_to player_path
      else
        redirect_to connections_start_path
      end

    end

  end

  def about
  end

  def contacts
  end

  def help
  end
end
