class CollectionsController < ApplicationController

  #rescue_from VkontakteApi::Error, with: :ignore_error
  before_action :authenticate_user!

  def soundcloud

    @user = current_user

    if @user.soundcloud_id?

      FirstSyncSoundcloudJob.perform_later @user
      @user.sync_sc = true
      @user.save

      render template: 'collections/index'

    else

      redirect_to connections_start_path

    end

  end

  def mixcloud

    @user = current_user

    if @user.mixcloud_id?

      FirstSyncMixcloudJob.perform_later @user
      @user.sync_mc = true
      @user.save

      render template: 'collections/index'

    else

      redirect_to connections_start_path

    end

  end

  def vk
    @user = current_user

    if @user.vk_id?

      FirstSyncVkJob.perform_now @user
      @user.sync_vk = true
      @user.save

      render template: 'collections/index'

    else

      redirect_to connections_start_path

    end

  end

  private

  def ignore_error
    puts '======================================='
    puts 'ooops, error! :)'
    puts '======================================='
  end

end
