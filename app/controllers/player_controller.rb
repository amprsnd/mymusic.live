class PlayerController < ApplicationController
  layout 'player'
  before_action :authenticate_user!

  def index
    @user = current_user
  end

  def folders
    @user = current_user

    @folders_three = [{id: 'a', value: 'All tracks', data: ''}]
    folders = @user.folders.all

    folders.each do |folder|
      unless folder.supfolder.present?
        single_folder = {id: folder.id, value: folder.title, data: sub_folders(folder)}
        @folders_three << single_folder
      end

    end

    respond_to do |format|

      #format.html
      format.json { render json: @folders_three }

    end

  end

  def tracks

    @user = current_user
    @tracks = []
    folder = params[:folder]

    if folder.present?

      if folder == 'a'
        # All tracks folder
        @tracks = @user.tracks.all.order('user_tracks.created_at DESC')
      else

        # tracks in folders
        # TODO: Do refactor

        current_folder = Folder.find_by id: folder

        if current_folder.subfolder.present?

          folders = sub_folders_tracks folder

          folders.each do |single_folder_id|

            single_folder = Folder.find_by id: single_folder_id

            if single_folder.tracks.present?

              single_folder.tracks.each do |track|

                @tracks << track

              end
            end
          end

        else

          current_folder.tracks.each do |track|
            @tracks << track
          end

        end

      end

    else
      # No folder select
      @tracks = @user.tracks.order('user_tracks.created_at DESC').limit(100)
    end



    @formatted_tracks = []

    @tracks.each do |track|

      cover = track.track_artwork_url ? track.track_artwork_url : '/images/cover.png'

      if track.track_source == 'soundCloud'
        track_time = duration (track.track_duration / 1000)
      else
        track_time = duration(track.track_duration)
      end

      track_order = UserTrack.where(user_id: @user.id, track_id: track.id).first

      track_order.present? ? order_crutch = track_order.updated_at : order_crutch = 999


      # puts ap track_order

      single_track = {
          id: track.id,
          track_id: track.id,
          outer_id: track.track_id,
          artist: track.track_artist,
          title: track.track_title,
          duration: track_time,
          artwork: cover,
          source: track.track_source.downcase,
          track_url: track.track_uri,
          sort: order_crutch,
          owner: track.track_artist_url
      }
      @formatted_tracks << single_track
    end

    # JSON respond
    respond_to do |format|
      format.json { render json: @formatted_tracks }
    end

  end

  def queue

    @user = current_user

    if request.post?
      # get list from user

      @queue = JSON.parse params[:queue]

      @user.play_queue.destroy_all

      @queue.each do |q|

        queue_track = q[1]

        if @user.track_ids.include? queue_track['track_id']

          track = Track.find_by(id: queue_track['track_id'])

          PlayQueue.create(
              track_id: track.id,
              outer_id: track.track_id,
              user_id: @user.id,
              track_order: queue_track['index'],
              track_url: track.track_uri,
              track_artist: track.track_artist,
              track_title: track.track_title,
              track_source: track.track_source,
              track_duration: track.track_duration
          )

          #puts ap "user: #{@user.id} / id: #{queue_track['track_id']} / index: #{queue_track['index']}"

        end

      end

      respond_to do |format|
        format.json { render json: 'post completed' }
      end

    else
      # send list to user

      @tracks = @user.play_queue.order(track_order: :asc)

      @formatted_tracks = []

      @tracks.each do |track|

        if track.track_source == 'soundCloud'
          track_time = duration (track.track_duration / 1000)
        else
          track_time = duration (track.track_duration)
        end

        single_track = {
            id: track.id,
            track_id: track.track_id,
            outer_id: track.outer_id,
            artist: track.track_artist,
            title: track.track_title,
            duration: track_time,
            track_url: track.track_url,
            source: track.track_source.downcase
        }
        @formatted_tracks << single_track
      end

      # JSON respond
      respond_to do |format|
        format.json { render json: @formatted_tracks }
      end

    end

  end

  private

  def sub_folders(folder)
    data = []
    if folder.subfolder.present?
      folder.subfolder.each do |subfolder|
        single_subfolder = {id: subfolder.id, value: subfolder.title, data: sub_folders(subfolder)}
        data << single_subfolder
      end
    end

    data
  end

  def sub_folders_tracks(folder)
    current_folder = Folder.find_by id: folder
    data = []
    if current_folder.subfolder.present?
      current_folder.subfolder.each do |subfolder|
        data << subfolder.id
      end
    end

    data
  end

  def duration(value)
    Time.at(value).utc.strftime('%H:%M:%S')
  end

end
