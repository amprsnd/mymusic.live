class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception




  #after login/logout paths
  def after_sign_in_path_for(resource)
    root_path
  end

  def after_sign_out_path_for(resource)
    root_path
  end

  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  #for devise
  if controller_name == 'devise'
    layout 'devise'
  end

  #for dashboard
  private
  def check_admin

    if current_user == nil || !current_user.admin?
      render file: "#{Rails.root}/public/404.html",
             status: 404,
             layout: false
    end

  end

end
