class UsersController < ApplicationController

  before_action :authenticate_user!

  def index
    @users = User.all
  end

  def profile
    @user = current_user

    client = SoundCloud.new(:access_token => @user.soundcloud_token)
    @client = client
    #http://api.soundcloud.com/users/71611913/favorites?client_id=bb2c55dd7bc8bc59ad98863c0c39bd8c
    @ololo = client.get("/me").public_favorites_count

    @act = client.get("/me/activities")

    @sc_favs = client.get('/me/favorites',  order: 'created_at',
                                            limit: 1,
                                            linked_partitioning: 1
                                            )


    @sc_favscollection = client.get('/me/favorites',  order: 'created_at',
                                                      limit: 100,
                                                      linked_partitioning: 1
                                                      )




  end

  def show
    @user = current_user
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
  end

  private


end
