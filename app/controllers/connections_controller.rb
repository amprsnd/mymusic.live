class ConnectionsController < ApplicationController

  before_action :authenticate_user!
  include HTTParty

  def start

  end

  def soundcloud

    @user = current_user

    app_id = 'bb2c55dd7bc8bc59ad98863c0c39bd8c'
    app_secret = 'c65cf3f8b3f5e7814573f0d42d35e06d'
    app_back_uri = 'http://mcloud.live/connections/soundcloud_auth'

    client = Soundcloud.new(client_id: app_id, client_secret: app_secret, redirect_uri: app_back_uri)

    code = params[:code]
    unless code.present?
      # redirect user to authorize URL
      redirect_to client.authorize_url()
    end

  end

  def soundcloud_token

    if request.post?

      @user = current_user
      client = Soundcloud.new(:access_token => params[:access_token])
      me = client.get('/me')

      @user.update_attributes(soundcloud_id: me['id'],
                              soundcloud_name: me['username'],
                              soundcloud_userpic: me['avatar_url'],
                              soundcloud_token: params[:access_token]
      )

      redirect_to collections_soundcloud_path
    end

  end

  def mixcloud

    @user = current_user
    app_api = 'https://api.mixcloud.com'
    app_oauth = 'https://www.mixcloud.com/oauth'

    app_id = 'cPpZ9cNd79eLLmEQuJ'
    app_secret = 'bYN6sFA2uNhYk5NmT8NHWQUSjXsV33Rv'
    app_back_uri = 'http://mcloud.live/connections/mixcloud'

    code = params[:code]

    if code.present?

      response = HTTParty.get("#{app_oauth}/access_token?client_id=#{app_id}&redirect_uri=#{app_back_uri}&client_secret=#{app_secret}&code=#{code}")

      access_token = response.parsed_response['access_token']
      me = HTTParty.get("#{app_api}/me/?access_token=#{access_token}").parsed_response
      me = JSON.parse me

      @user.update_attributes(mixcloud_id: me['key'],
                              mixcloud_name: me['name'],
                              mixcloud_userpic: me['pictures']['320wx320h'],
                              mixcloud_token: access_token
      )

      redirect_to collections_mixcloud_path

    else
      # redirect user to authorize URL
      redirect_to("#{app_oauth}/authorize?client_id=#{app_id}&redirect_uri=#{app_back_uri}")
    end



  end

  def vk

    @user = current_user

    VkontakteApi.configure do |config|
      config.app_id       = '5443672'
      config.app_secret   = 'MqCTHnYB2oshjTqJCjwB'
      config.redirect_uri = 'http://mcloud.live/connections/vk'

      config.adapter = :net_http
      config.http_verb = :get
      config.max_retries = 3

      config.logger        = Rails.logger
      config.log_requests  = true  # URL-ы запросов
      config.log_errors    = true  # ошибки
      config.log_responses = false # удачные ответы

      #config.api_version = '5.21'
    end

    code = params[:code]

    if code.present? && params[:state] == session[:state]

      @vk = VkontakteApi.authorize(code: code)

      @user.vk_id = @vk.user_id
      @user.vk_token = @vk.token
      #@user.vk_name = "#{@vk.first_name} #{@vk.last_name}"
      #@user.vk_userpic = @vk.photo_100
      @user.save

      redirect_to collections_vk_path

    else

      session[:state] = Digest::MD5.hexdigest(rand.to_s)
      redirect_to VkontakteApi.authorization_url(scope: [:audio, :groups, :offline], state: session[:state])

    end

  end
end
