class CreateJoinTableTrackFolder < ActiveRecord::Migration
  def change
    create_join_table :tracks, :folders do |t|
      t.index [:track_id, :folder_id], name: 'track_folder', unique: true
      t.index [:folder_id, :track_id], name: 'folder_track', unique: true
    end
  end
end
