class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.string :title
      t.string :color
      t.string :label
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
