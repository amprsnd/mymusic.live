class ChangeTrackTypeColumnName < ActiveRecord::Migration
  def change
    rename_column :tracks, :type, :track_source
  end
end
