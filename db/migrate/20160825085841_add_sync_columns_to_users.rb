class AddSyncColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :sync_sc, :boolean
    add_column :users, :sync_mc, :boolean
    add_column :users, :sync_vk, :boolean
  end
end
