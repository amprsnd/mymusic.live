class CreatePlayQueues < ActiveRecord::Migration
  def change
    create_table :play_queues do |t|
      t.references :track, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :track_order
      t.string :track_url
      t.string :track_source
      t.integer :track_duration

      t.timestamps null: false
    end
  end
end
