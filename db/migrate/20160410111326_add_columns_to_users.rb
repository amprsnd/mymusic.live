class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :soundcloud_token, :string
    add_column :users, :soundcloud_id, :string
    add_column :users, :soundcloud_name, :string
    add_column :users, :soundcloud_userpic, :string
  end
end
