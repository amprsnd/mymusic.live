class AddMixcloudToUsers < ActiveRecord::Migration
  def change
    add_column :users, :mixcloud_id, :string
    add_column :users, :mixcloud_name, :string
    add_column :users, :mixcloud_userpic, :string
    add_column :users, :mixcloud_token, :string
  end
end
