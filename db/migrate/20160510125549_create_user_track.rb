class CreateUserTrack < ActiveRecord::Migration
  def change
    create_table :user_tracks do |t|
      t.integer :user_id
      t.integer :track_id
      t.string :mood
      t.string :times_of_day
      t.string :weather
      t.string :color
      t.index [:user_id, :track_id], name: 'user_track', unique: true

      t.timestamps null: false

      #add_index :user_tracks, :user_id
      #add_index :user_tracks, :track_id
    end
  end
end
