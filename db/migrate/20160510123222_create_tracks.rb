class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.integer :track_id
      t.datetime :track_created_at
      t.datetime :track_updated_at
      t.integer :track_user_id
      t.integer :track_duration
      t.boolean :track_commentable
      t.string :track_state
      t.integer :track_original_content_size
      t.string :track_sharing
      t.string :track_tag_list, limit: 512
      t.string :track_permalink
      t.boolean :track_streamable
      t.string :track_embeddable_by
      t.boolean :track_downloadable
      t.string :track_purchase_url
      t.string :track_label_id
      t.string :track_purchase_title
      t.string :track_genre
      t.string :track_title
      t.string :track_description, limit: 512
      t.string :track_label_name
      t.string :track_release
      t.string :track_track_type
      t.string :track_key_signature
      t.string :track_isrc
      t.string :track_video_url
      t.string :track_bpm
      t.integer :track_release_year
      t.integer :track_release_month
      t.integer :track_release_day
      t.string :track_original_format
      t.string :track_license
      t.string :track_uri
      t.string :track_permalink_url
      t.string :track_artwork_url
      t.string :track_waveform_url
      t.string :track_stream_url
      t.string :track_attachments_uri
      t.string :track_owner
      t.string :track_artist
      t.string :track_artist_userpic
      t.string :track_artist_url
      t.integer :track_count_play
      t.integer :track_count_favorite
      t.integer :track_count_listener
      t.integer :track_count_repost
      t.integer :track_count_comment
      t.string :type

      t.timestamps null: false

      #add_index :tracks, :track_id
    end
  end
end
