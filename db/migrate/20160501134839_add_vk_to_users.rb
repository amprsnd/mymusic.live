class AddVkToUsers < ActiveRecord::Migration
  def change
    add_column :users, :vk_id, :string
    add_column :users, :vk_token, :string
    add_column :users, :vk_name, :string
    add_column :users, :vk_userpic, :string
  end
end
