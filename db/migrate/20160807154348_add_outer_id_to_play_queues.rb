class AddOuterIdToPlayQueues < ActiveRecord::Migration
  def change
    add_column :play_queues, :outer_id, :string
  end
end
