class AddColumnsToPlayQueues < ActiveRecord::Migration
  def change
    add_column :play_queues, :track_artist, :string
    add_column :play_queues, :track_title, :string
  end
end
