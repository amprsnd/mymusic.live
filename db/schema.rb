# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160825085841) do

  create_table "folders", force: :cascade do |t|
    t.string   "title",        limit: 255
    t.string   "color",        limit: 255
    t.string   "label",        limit: 255
    t.integer  "user_id",      limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "supfolder_id", limit: 4
  end

  add_index "folders", ["user_id"], name: "index_folders_on_user_id", using: :btree

  create_table "folders_tracks", id: false, force: :cascade do |t|
    t.integer "track_id",  limit: 4, null: false
    t.integer "folder_id", limit: 4, null: false
  end

  add_index "folders_tracks", ["folder_id", "track_id"], name: "folder_track", unique: true, using: :btree
  add_index "folders_tracks", ["track_id", "folder_id"], name: "track_folder", unique: true, using: :btree

  create_table "play_queues", force: :cascade do |t|
    t.integer  "track_id",       limit: 4
    t.integer  "user_id",        limit: 4
    t.integer  "track_order",    limit: 4
    t.string   "track_url",      limit: 255
    t.string   "track_source",   limit: 255
    t.integer  "track_duration", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "track_artist",   limit: 255
    t.string   "track_title",    limit: 255
    t.string   "outer_id",       limit: 255
  end

  add_index "play_queues", ["track_id"], name: "index_play_queues_on_track_id", using: :btree
  add_index "play_queues", ["user_id"], name: "index_play_queues_on_user_id", using: :btree

  create_table "playlists", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.string   "description", limit: 255
    t.integer  "user_id",     limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "tracks", force: :cascade do |t|
    t.integer  "track_id",                    limit: 4
    t.datetime "track_created_at"
    t.datetime "track_updated_at"
    t.integer  "track_user_id",               limit: 4
    t.integer  "track_duration",              limit: 4
    t.boolean  "track_commentable"
    t.string   "track_state",                 limit: 255
    t.integer  "track_original_content_size", limit: 4
    t.string   "track_sharing",               limit: 255
    t.string   "track_tag_list",              limit: 512
    t.string   "track_permalink",             limit: 255
    t.boolean  "track_streamable"
    t.string   "track_embeddable_by",         limit: 255
    t.boolean  "track_downloadable"
    t.string   "track_purchase_url",          limit: 255
    t.string   "track_label_id",              limit: 255
    t.string   "track_purchase_title",        limit: 255
    t.string   "track_genre",                 limit: 255
    t.string   "track_title",                 limit: 255
    t.string   "track_description",           limit: 512
    t.string   "track_label_name",            limit: 255
    t.string   "track_release",               limit: 255
    t.string   "track_track_type",            limit: 255
    t.string   "track_key_signature",         limit: 255
    t.string   "track_isrc",                  limit: 255
    t.string   "track_video_url",             limit: 255
    t.string   "track_bpm",                   limit: 255
    t.integer  "track_release_year",          limit: 4
    t.integer  "track_release_month",         limit: 4
    t.integer  "track_release_day",           limit: 4
    t.string   "track_original_format",       limit: 255
    t.string   "track_license",               limit: 255
    t.string   "track_uri",                   limit: 255
    t.string   "track_permalink_url",         limit: 255
    t.string   "track_artwork_url",           limit: 255
    t.string   "track_waveform_url",          limit: 255
    t.string   "track_stream_url",            limit: 255
    t.string   "track_attachments_uri",       limit: 255
    t.string   "track_owner",                 limit: 255
    t.string   "track_artist",                limit: 255
    t.string   "track_artist_userpic",        limit: 255
    t.string   "track_artist_url",            limit: 255
    t.integer  "track_count_play",            limit: 4
    t.integer  "track_count_favorite",        limit: 4
    t.integer  "track_count_listener",        limit: 4
    t.integer  "track_count_repost",          limit: 4
    t.integer  "track_count_comment",         limit: 4
    t.string   "track_source",                limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "artwork_file_name",           limit: 255
    t.string   "artwork_content_type",        limit: 255
    t.integer  "artwork_file_size",           limit: 4
    t.datetime "artwork_updated_at"
  end

  create_table "user_tracks", force: :cascade do |t|
    t.integer  "user_id",      limit: 4
    t.integer  "track_id",     limit: 4
    t.string   "mood",         limit: 255
    t.string   "times_of_day", limit: 255
    t.string   "weather",      limit: 255
    t.string   "color",        limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "user_tracks", ["user_id", "track_id"], name: "user_track", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 100, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "soundcloud_token",       limit: 255
    t.string   "soundcloud_id",          limit: 255
    t.string   "soundcloud_name",        limit: 255
    t.string   "soundcloud_userpic",     limit: 255
    t.string   "mixcloud_id",            limit: 255
    t.string   "mixcloud_name",          limit: 255
    t.string   "mixcloud_userpic",       limit: 255
    t.string   "mixcloud_token",         limit: 255
    t.string   "vk_id",                  limit: 255
    t.string   "vk_token",               limit: 255
    t.string   "vk_name",                limit: 255
    t.string   "vk_userpic",             limit: 255
    t.boolean  "admin"
    t.boolean  "sync_sc"
    t.boolean  "sync_mc"
    t.boolean  "sync_vk"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  add_foreign_key "folders", "users"
  add_foreign_key "play_queues", "tracks"
  add_foreign_key "play_queues", "users"
end
