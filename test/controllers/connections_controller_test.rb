require 'test_helper'

class ConnectionsControllerTest < ActionController::TestCase
  test "should get soundcloud" do
    get :soundcloud
    assert_response :success
  end

  test "should get mixcloud" do
    get :mixcloud
    assert_response :success
  end

  test "should get vk" do
    get :vk
    assert_response :success
  end

end
