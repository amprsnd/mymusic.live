require 'test_helper'

class PlayerControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get folders" do
    get :folders
    assert_response :success
  end

  test "should get tracks" do
    get :tracks
    assert_response :success
  end

  test "should get queue" do
    get :queue
    assert_response :success
  end

end
