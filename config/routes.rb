Rails.application.routes.draw do

  get 'dashboard/index'

  #satatic
  root 'static#index'
  get 'about' => 'static#about'
  #get 'help' => 'static#help'
  #get 'contacts' => 'static#contacts'

  devise_for :users, path: 'auth', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    password: 'secret',
    confirmation: 'verification',
    unlock: 'unblock',
    registration: 'register',
    sign_up: 'connect'
  }

  # Authorize profiles
  get   'connections/start'
  get 'connections/soundcloud'
  match 'connections/soundcloud_auth', to: 'connections#soundcloud_token', via: [:get, :post]
  get   'connections/mixcloud'
  get   'connections/vk'

  # Collect data
  get 'collections/soundcloud'
  get 'collections/mixcloud'
  get 'collections/vk'

  # resources :users, only: [:index, :show, :edit, :update]
  # get '/profile', to: 'users#profile', as: 'profile'

  # player
  get 'player' => 'player#index'
  get 'player/folders'
  get 'player/tracks'
  match 'player/queue', to: 'player#queue', via: [:get, :post]

  # admin area

  get 'dashboard' => 'dashboard#index'
  namespace :dashboard do

    resources :users

    authenticate :user, lambda {|u| u.admin? } do
      #mount Resque::Server, at: '/jobs'
    end

  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
